import json
import logging
import re
import threading
import traceback
from copy import deepcopy
from random import choice as _random_choice
from string import ascii_uppercase
from time import sleep

import requests

logging.basicConfig(level=logging.INFO, format='%(asctime)s : %(levelname)s - %(message)s')

_problems = None


class UpdateProblemsThread(threading.Thread):

    def run(self):
        global _problems
        while True:
            json = requests.get('https://kenkoooo.com/atcoder/resources/merged-problems.json')
            _problems = json.json()
            logging.info('Problem Updated')
            sleep(60 * 60)
            # sleep(2 * 60)


UpdateProblemsThread().start()


def filter_content_type(problem, content_type) -> bool:
    contest_id = problem['contest_id']
    res = 6 <= len(contest_id) <= 7 and \
        contest_id[:3] == content_type and \
        contest_id[3].isdigit()

    return res


def filter_index(problem, index) -> bool:
    return problem['problem_index'] == index


def filter_index(problem, index) -> bool:
    return problem['problem_index'] == index


def filter_twitter(word):
    return word in ('abc', 'arc', 'agc') or \
            word.upper() in ascii_uppercase


def get_problem(content_type=None, index=None):
    problems = deepcopy(_problems)
    if content_type is not None:
        problems = [p for p in problems if filter_content_type(p, content_type)]
    if index is not None:
        problems = [p for p in problems if filter_index(p, index)]

    if problems == []:
        return None
    else:
        return _random_choice(problems)


def get_url(problem) -> str:
    return f'https://atcoder.jp/contests/{problem["contest_id"]}/tasks/{problem["id"]}'


import os
from os.path import dirname, join

import tweepy
from dotenv import load_dotenv

DOTENV_PATH = join(dirname(__file__), '.env')
load_dotenv(DOTENV_PATH)

TWITTER_API_KEY = os.getenv('TWITTER_API_KEY')
TWITTER_API_SECRET_KEY = os.getenv('TWITTER_API_SECRET_KEY')
TWITTER_API_ACCESS_TOKEN = os.getenv('TWITTER_API_ACCESS_TOKEN')
TWITTER_API_ACCESS_TOKEN_SECRET = os.getenv('TWITTER_API_ACCESS_TOKEN_SECRET')
TWITTER_BEARER_TOKEN = os.getenv('TWITTER_BEARER_TOKEN')
TWITTER_USERID = os.getenv('TWITTER_USERID')

client = tweepy.Client(
    TWITTER_BEARER_TOKEN, TWITTER_API_KEY, TWITTER_API_SECRET_KEY, TWITTER_API_ACCESS_TOKEN,
    TWITTER_API_ACCESS_TOKEN_SECRET)


class Listener(tweepy.StreamingClient):

    def on_data(self, raw_data):
        try:
            json_response = json.loads(raw_data)
            tweet_id = json_response["data"]["id"]  #ツイートID

            logging.info(json_response)
            text = re.sub(r'@\S+', '', json_response['data']['text'])
            text = text.strip()
            conds = re.sub(r'\s+', ' ', text)
            conds = re.split(r'\s+', conds)
            conds = [c for c in conds if filter_twitter(c)]
            content_type = None
            index = None
            for t in conds:
                if t in ('abc', 'arc', 'agc'):
                    if content_type is None:
                        content_type = t
                    else:
                        continue
                elif t.upper() in ascii_uppercase:
                    if index is None:
                        index = t.upper()
                    else:
                        continue
                else:
                    logging.error(f'{t} is not valid word.')
                    return

            if content_type is None and index is None:
                return

            problem = get_problem(content_type, index)

            if problem is not None:
                client.create_tweet(text=get_url(problem), in_reply_to_tweet_id=tweet_id)
        except Exception as e:
            logging.error(traceback.format_exc())


if __name__ == '__main__':
    printer = Listener(TWITTER_BEARER_TOKEN)
    printer.add_rules(tweepy.StreamRule(TWITTER_USERID))
    printer.filter()
